hagyma kepfeltoltos szara
=========================

1. user feltolt egy mappaba n darab kepet
2. user feltolt egy info.txt filet ami a kepekre kerulo infot tartalmazza (3sor)
3. a progi figyeli, hogy megjottek-e a fileok es van-e info.txt. ha igen akkor nekilendul
4. atmertezi a kepeket megfelelo meretre
5. rapakol egy shadinget
6. rapakol egy logot
7. rapakolja az info.txt (1. sor bold, 2. sor normal es 3. sor small )
8. lementi egy mappaba
9. osszezipeli
10. kizuzza mailben adott cimzetteknek

Ez lehet egy buta tool, de lehet egy valamifele webes interfesszel rendelkezo akarmi is. Ez utobbi eseteben feltoltogetes van es az info txt helyett beirkalas.
Az fasza ha van valamifele log vagy egyeb es meg kelene tartani az eredeti kepeket is.

Ubuntu install
==============

1. sudo apt-get install python python-dev python-pip
2. pip install virtualenv
3. virtualenv env && source env/bin/activate
4. pip install -r requirements.txt
5. mysql> create database hockey;
6. mysql> grant usage on *.* to hockey@localhost identified by 'BhmN3pNkTU5q';
7. manage.py syncdb
8. set up nginx and uwsgi, wsgi file is hagyma/wsgi.py
9. enable email sending

Help: https://docs.djangoproject.com/en/dev/howto/deployment/
Settings: hagyma/settings.py
