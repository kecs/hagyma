#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from time import gmtime, strftime
from PIL import Image, ImageFont, ImageDraw
from django.core.files import File
from django.conf import settings
from django.db import models
from custom_user.models import AbstractEmailUser
from django.dispatch import receiver
from django.db.models.signals import post_save

        
class MyEmailUser(AbstractEmailUser):
    name = models.CharField(max_length=255)
    
    class Meta:
        verbose_name = 'user'


class Album(models.Model):
    date = models.DateTimeField()
    headline = models.CharField(max_length=255)
    score = models.CharField(max_length=125)
    photographer = models.CharField(max_length=255, blank=True, default='')
    token = models.CharField(max_length=16, blank=True, default='')
    
    created_at = models.DateTimeField(auto_now_add=True)
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    
    def get_absolute_url(self):
        return "album/%d/" % self.id

    def save(self, *args, **kwargs):
        " Auto set photographers name "
        
        if not self.photographer:
            self.photographer = self.user.name
            
        return super(Album, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-created_at']

        
class IMG(models.Model):
    SIZE_X = 700
    SIZE_Y = 460
    DIR = settings.BASE_DIR + '/app/assets/'
    
    src_orig = models.ImageField(upload_to='uploads')
    src_mod = models.ImageField(
        upload_to='uploads', blank=True, null=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    album = models.ForeignKey(Album, related_name='images',
        blank=True, null=True)

    class Meta:
        ordering = ['-uploaded_at']

    def get_absolute_url(self):
        return '/' + self.src_orig.url.replace('uploads', 'static')
        
    def get_mod_url(self):
        return '/' + self.src_mod.url.replace('uploads', 'static')


@receiver(post_save, sender=IMG)
def set_src_mod(sender, instance=None, created=False, **kwargs):
    if not created:
        return

    mod_name = '%s/mol_%s' % ( os.path.dirname(instance.src_orig.name), os.path.basename(instance.src_orig.name) )
    instance.src_mod.save(
        mod_name,
        instance.src_orig,
    )
    
    img = Image.open(instance.src_mod)
    logo = Image.open(IMG.DIR + '3_logo.png')
    shadow = Image.open(IMG.DIR + '2_shady.png')
    font_1 = ImageFont.truetype(IMG.DIR + "arialbd.ttf", size=14)
    font_2 = ImageFont.truetype(IMG.DIR + "arial.ttf", size=12)
    font_3 = ImageFont.truetype(IMG.DIR + "arial.ttf", size=10)
        
    img = img.resize((IMG.SIZE_X, IMG.SIZE_Y), Image.ANTIALIAS)
    draw = ImageDraw.Draw(img)

    img.paste(
        shadow,
        (0, img.size[1] - shadow.size[1]),
        shadow
    )
    img.paste(
        logo,
        (10, img.size[1] - logo.size[1] - 10),
        logo
    )

    draw.text(
        (90, img.size[1] - 60),
        instance.album.headline,
        font=font_1
    )
    draw.text(
        (90, img.size[1] - 40),
        instance.album.score,
        font=font_2
    )
    draw.text(
        (90, img.size[1] - 20),
        "%s FOTO: %s" % (
            instance.album.date.strftime('%Y/%M/%d'),
            instance.album.photographer.upper(),
        ),
        font=font_3
    )

    del draw

    quality_val = 90
    img.save(mod_name, 'JPEG', quality=quality_val)

    
class Recipient(models.Model):
    """Singleton"""
    
    def save(self, *args, **kwargs):
        recipients = Recipient.objects.all()
        if self.id:
            return super(Recipient, self).save(*args, **kwargs)
        else:    
            return False
    
    def __unicode__(self):
        return ', '.join(unicode(s) for s in self.send_to.all()[:5]) + '...'
    
    
class SendTo(models.Model):
    address = models.EmailField()
    recipient = models.ForeignKey(Recipient, related_name='send_to', blank=True, null=True)
    
    class Meta:
        verbose_name = 'Email address'
    
    def __unicode__(self):
        return self.address
    
    
