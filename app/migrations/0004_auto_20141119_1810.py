# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20141119_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sendto',
            name='recipient',
            field=models.ForeignKey(related_name=b'send_to', blank=True, to='app.Recipient', null=True),
        ),
    ]
