#!/usr/bin/env python
# -*- coding: utf-8 -*-

import commands
import hashlib
from random import random
from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView
from django.forms.models import modelformset_factory
from django.utils import timezone
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth import authenticate, login
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.utils.text import slugify
from django.core.mail import EmailMessage
from django.core.files import File
from app.models import *
from .forms import LoginForm, AlbumForm, IMGForm


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class MenuMixin(object):
    def get_context_data(self, **kwargs):
        context = super(MenuMixin, self).get_context_data(**kwargs)
        context['menu'] = getattr(self, 'menu', 'index')
        return context


class AlbumListView(LoginRequiredMixin, MenuMixin, ListView):
    model = Album
    template_name = 'list.html'
    menu = 'index'


class AlbumCreateView(LoginRequiredMixin, MenuMixin, CreateView):
    model = Album
    template_name = 'album_edit.html'
    form_class = AlbumForm
    menu = 'list'
    success_url = '/'


class AlbumUpdateView(LoginRequiredMixin, UpdateView):
    model = Album
    template_name = 'album_edit.html'
    form_class = AlbumForm

    def get_success_url(self):
        return '/album/%d/' % self.object.id


class AlbumDetailView(LoginRequiredMixin, DetailView):
    model = Album
    template_name = 'detail.html'
    exclude = ['id']

    def get_context_data(self, **kwargs):
        context = super(AlbumDetailView, self).get_context_data(**kwargs)
        context['imgs'] = self.object.images.all()

        return context


class IMGCreateView(LoginRequiredMixin, View):
    model = IMG
    template_name = 'upload.html'
    form_class = IMGForm

    def dispatch(self, request, **kwargs):
        self.album = get_object_or_404(Album, pk=kwargs.pop('album_pk'))

        return super(IMGCreateView, self).dispatch(request, **kwargs)

    def post(self, request):
        for f in request.FILES.getlist('src_orig'):
            img = self.model(album=self.album)
            img.src_orig.save(f.name, f)
            img.save()

        return HttpResponseRedirect(self.get_success_url())

    def get(self, request):
        return render_to_response(
            self.template_name,
            {'object': self.album, 'form': self.form_class()},
            context_instance=RequestContext(request)
        )

    def get_success_url(self):
        return '/album/%s' % self.album.id


class AlbumDeleteView(LoginRequiredMixin, DeleteView):
    model = Album
    success_url = '/'
    template_name = 'album_confirm_delete.html'


class LoginView(FormView):
    model = Album
    template_name = 'login.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        user = authenticate(
            email=form.cleaned_data['email'],
            password=form.cleaned_data['password']
        )

        if user:
            login(self.request, user)

        return super(LoginView, self).form_valid(form)


class IMGSendView(View):
    def post(self, request, *args, **kwargs):
        album = get_object_or_404(Album, pk=kwargs.get('pk', 0))

        attachment_name = '%d-%d-%d-%s-%s' % (
            album.date.year,
            album.date.month,
            album.date.day,
            slugify(album.headline),
            album.token
        )

        attachment_path = '%s/uploads/' % settings.BASE_DIR

        msg = 'Did not manage to send. Call the IT guy.'
        with open('%s%s.zip' % (attachment_path, attachment_name), 'r') as f:
            msg = 'Zipped, mail sent, wait for it.'

            recipients = [request.user.email]
            for r in Recipient.objects.all():
                for s in r.send_to.all():
                    recipients += [s.address]
				
            title = 'Iceman Imaging %d-%d-%d' % (
                album.date.year,
                album.date.month,
                album.date.day
            )
            mail = EmailMessage(
                title,
                'http://%s/static/%s.zip' % (request.META['HTTP_HOST'], attachment_name),
                settings.DEFAULT_FROM_EMAIL,
                recipients
            )
            mail.attach(attachment_name, f.read(), 'application/zip')
            mail.send()

        return render_to_response(
            'success.html',
            {'msg': msg},
            context_instance=RequestContext(request)
        )

class IMGZipView(View):
    def post(self, request, *args, **kwargs):
        album = get_object_or_404(Album, pk=kwargs.get('pk', 0))
        token = hashlib.md5(str(random())).hexdigest()[:16]

        attachment_name = '%d-%d-%d-%s-%s' % (
            album.date.year,
            album.date.month,
            album.date.day,
            slugify(album.headline),
            token,
        )

        attachment_path = '%s/uploads/' % settings.BASE_DIR

        cmd = 'cd %s && zip %s ' % (
            attachment_path,
            attachment_name,
        )

        for img in album.images.all():
            cmd += os.path.basename(img.src_orig.url) + ' '
            cmd += os.path.basename(img.src_mod.url) + ' '

        status, output = commands.getstatusoutput(cmd)

        if status is 0:
            album.token = token
            album.save()

            return render_to_response(
                'success_zip.html',
                {
                    'album_id': album.id,
                    'msg': 'Copy and send the above URL:',
                    'zip_url': 'http://%s/static/%s.zip' % (request.META['HTTP_HOST'], attachment_name)
                },
                context_instance=RequestContext(request)
            )

        else:
            return render_to_response(
                'success_zip.html',
                {
                    'msg': 'Did not manage to zip and send.',
                },
                context_instance=RequestContext(request)
            )

