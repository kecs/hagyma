import re
import datetime
from django import forms
from django.core.exceptions import ValidationError
from app.models import *


class NoTZDateTimeField(forms.DateTimeField):
    """ Use HU time zone """
    
    def to_python(self, value):
        if not 'GMT+0200 (CEST)' in value:
            value += ' GMT+0200 (CEST)'

        return super(NoTZDateTimeField, self).to_python(value)

        
class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(max_length=100)
    

class AlbumForm(forms.ModelForm):
    date = NoTZDateTimeField(
        input_formats=['%a %b %d %Y %H:%M:%S GMT+0200 (CEST)']
    )
    
    class Meta:
        model = Album
        exclude = ['id', 'user', 'date']
        
    def clean_date(self):
        data = self.cleaned_data['date']
        self.instance.date = data
        return data

        
class IMGForm(forms.ModelForm):
    class Meta:
        model = IMG
        exclude = ['id', 'album', 'src_mod']
    
    def save(self, *args, **kwargs):
        return super(IMGForm, self).save(*args, **kwargs)
        

        
        
        
        
