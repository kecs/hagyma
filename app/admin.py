from django.contrib import admin
from django.contrib.auth.models import Group
from custom_user.admin import EmailUserAdmin
from .models import MyEmailUser, Recipient, SendTo


class MyEmailUserAdmin(EmailUserAdmin):
    pass


class SendToInline(admin.TabularInline):
    model = SendTo


class RecipientAdmin(admin.ModelAdmin):
    inlines = [SendToInline]

    
admin.site.register(Recipient, RecipientAdmin)
admin.site.unregister(Group)
admin.site.register(MyEmailUser, MyEmailUserAdmin)
