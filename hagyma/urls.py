from django.conf.urls import patterns, include, url
from django.contrib import admin
from app.views import *
from django.conf.urls.static import static
from django.conf import settings


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', AlbumListView.as_view(), name='index'),
    url(r'^create$', AlbumCreateView.as_view(), name='create'),
    url(r'^login', LoginView.as_view(), name='login'),
    url(r'^album/(?P<pk>\d+)/$', AlbumDetailView.as_view(), name='detail'),
    url(r'^album/(?P<album_pk>\d+)/upload/$', IMGCreateView.as_view(), name='img_create'),
    url(r'^album/edit/(?P<pk>\d+)/$', AlbumUpdateView.as_view(), name='update'),
    url(r'^album/delete/(?P<pk>\d+)/$', AlbumDeleteView.as_view(), name='delete_album'),
    url(r'^album/zip/(?P<pk>\d+)/$', IMGZipView.as_view(), name='zip_url'),
    url(r'^album/send/(?P<pk>\d+)/$', IMGSendView.as_view(), name='send'),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    import debug_toolbar
    
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT
    )
    
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
